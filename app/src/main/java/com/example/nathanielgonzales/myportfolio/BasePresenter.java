package com.example.nathanielgonzales.myportfolio;

public interface BasePresenter {

    void start();

}
