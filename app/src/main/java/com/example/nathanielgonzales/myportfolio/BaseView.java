package com.example.nathanielgonzales.myportfolio;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
