package com.example.nathanielgonzales.myportfolio.Model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BackgroundPOJO {
    @Expose
    @SerializedName("email")
    private String email;
    @Expose
    @SerializedName("lastname")
    private String lastname;
    @Expose
    @SerializedName("firstname")
    private String firstname;
    @Expose
    @SerializedName("nickName")
    private String nickName;
    @Expose
    @SerializedName("description")
    private String description;
    @Expose
    @SerializedName("age")
    private Long age;
    @Expose
    @SerializedName("contactNumber")
    private String contactNumber;
    @Expose
    @SerializedName("position")
    private String position;
    @Expose
    @SerializedName("quotation")
    private String quotation;

    public BackgroundPOJO(String email, String lastname, String firstname, String nickName, String description, Long age, String contactNumber, String position, String quotation){
        this.email = email;
        this.lastname = lastname;
        this.firstname = firstname;
        this.nickName = nickName;
        this.description = description;
        this.age = age;
        this.contactNumber = contactNumber;
        this.position = position;
        this.quotation = quotation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getQuotation(){ return quotation;}
    public void setQuotation(String quotation){this.quotation = quotation;}
}