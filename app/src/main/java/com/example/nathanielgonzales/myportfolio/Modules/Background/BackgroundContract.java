package com.example.nathanielgonzales.myportfolio.Modules.Background;

import com.example.nathanielgonzales.myportfolio.BasePresenter;
import com.example.nathanielgonzales.myportfolio.BaseView;
import com.example.nathanielgonzales.myportfolio.Model.BackgroundPOJO;

import java.util.ArrayList;

public interface BackgroundContract {

    interface View extends BaseView<Presenter> {
        void setProgressIndicator(boolean active);
        void showStatistics(int numberOfIncompleteTasks, int numberOfCompletedTasks);
        void showLoadingStatisticsError();
        boolean isActive();
        void setDescription(String description);
        void setNickname(String nickname);
    }
    interface Presenter extends BasePresenter {
        void onDestroy();

        void getBackgroundDataFromAPI();
    }
    interface getBackgroundInteractor {
        interface OnFinishedListener {
            void onFinished(BackgroundPOJO backgroundPOJO);
            void onFailure(Throwable t);
        }

        void getBackgroundPOJO(OnFinishedListener onFinishListener);

        void getBackgroundData(OnFinishedListener onFinishedListener);
    }
}
