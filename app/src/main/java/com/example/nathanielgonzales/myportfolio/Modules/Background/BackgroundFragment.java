package com.example.nathanielgonzales.myportfolio.Modules.Background;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.example.nathanielgonzales.myportfolio.Model.BackgroundPOJO;
import com.example.nathanielgonzales.myportfolio.Modules.MainActivity.MainPresenter;
import com.example.nathanielgonzales.myportfolio.R;

import static android.support.v4.util.Preconditions.checkNotNull;

public class BackgroundFragment extends Fragment implements BackgroundContract.View {
    private BackgroundContract.Presenter presenter;
    BackgroundPOJO bpojo;
    public static BackgroundFragment newInstance() {
        return new BackgroundFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.background_fragment, container, false);

        presenter = new BackgroundPresenter(this, new BackgroundInteractor());
        presenter.getBackgroundDataFromAPI();



        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        presenter.start();
    }
    @Override
    public void setProgressIndicator(boolean active) {
    }
    @Override
    public void showStatistics(int numberOfIncompleteTasks, int numberOfCompletedTasks) {
    }
    @Override
    public void showLoadingStatisticsError() {
    }
    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setDescription(String description) {
            }

    @Override
    public void setNickname(String nickname) {

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setPresenter(BackgroundContract.Presenter presenter) {
        presenter = checkNotNull(presenter);
    }
}
