package com.example.nathanielgonzales.myportfolio.Modules.Background;

import android.util.Log;

import com.example.nathanielgonzales.myportfolio.Model.BackgroundPOJO;

public class BackgroundPresenter implements BackgroundContract.Presenter, BackgroundContract.getBackgroundInteractor.OnFinishedListener {
    private BackgroundContract.View mainView;
    private BackgroundContract.getBackgroundInteractor getBackgroundInteractor;
    @Override
    public void onDestroy() {
        mainView = null;
    }

    public BackgroundPresenter(BackgroundContract.View mainView, BackgroundContract.getBackgroundInteractor getBackgroundInteractor){
        this.mainView = mainView;
        this.getBackgroundInteractor = getBackgroundInteractor;
    }

    @Override
    public void getBackgroundDataFromAPI() {
        getBackgroundInteractor.getBackgroundPOJO(this);
    }

    @Override
    public void onFinished(BackgroundPOJO bpojo) {
        if(mainView != null){
            Log.d("AWW:", bpojo.getDescription()+"");
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(mainView != null){
        }
    }

    @Override
    public void start() {

    }
}
