package com.example.nathanielgonzales.myportfolio.Modules.MainActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nathanielgonzales.myportfolio.Model.BackgroundPOJO;
import com.example.nathanielgonzales.myportfolio.Modules.Background.BackgroundFragment;
import com.example.nathanielgonzales.myportfolio.Modules.Background.BackgroundInteractor;
import com.example.nathanielgonzales.myportfolio.Modules.Background.BackgroundPresenter;
import com.example.nathanielgonzales.myportfolio.Modules.ContactMe.ContactMeFragment;
import com.example.nathanielgonzales.myportfolio.Modules.Skills.SkillsFragment;
import com.example.nathanielgonzales.myportfolio.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainContract.MainView {
    private MainContract.presenter presenter;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private TextView txtDesc, txtNickName;
    private ProgressBar progressBar;
    BackgroundPOJO bpojo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        initializeObjects();
        initProgressBar();
        presenter = new MainPresenter(this, new MainInteractor());
        presenter.getBackgroundDataFromAPI();


    }
    private void initializeObjects() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.htab_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) getSupportActionBar().setTitle("");
        getSupportActionBar().hide();
        final ViewPager viewPager = (ViewPager) findViewById(R.id.htab_viewpager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.htab_tabs);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.htab_collapse_toolbar);
        txtDesc = findViewById(R.id.txtDescription);
        txtNickName = findViewById(R.id.txtNickname);
        tabLayout.setupWithViewPager(viewPager);
        setupViewPager(viewPager);

        try {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.header);
            Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                @SuppressWarnings("ResourceType")
                @Override
                public void onGenerated(Palette palette) {

                    int vibrantColor = palette.getVibrantColor(R.color.primary_500);
                    int vibrantDarkColor = palette.getDarkVibrantColor(R.color.primary_700);
                    collapsingToolbarLayout.setContentScrimColor(vibrantColor);
                    collapsingToolbarLayout.setStatusBarScrimColor(vibrantDarkColor);
                }
            });

        } catch (Exception e) {
            // if Bitmap fetch fails, fallback to primary colors
            collapsingToolbarLayout.setContentScrimColor(
                    ContextCompat.getColor(this, R.color.primary_500)
            );
            collapsingToolbarLayout.setStatusBarScrimColor(
                    ContextCompat.getColor(this, R.color.primary_700)
            );
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new BackgroundFragment(), "Background");
        adapter.addFrag(new SkillsFragment(), "Skills");
        adapter.addFrag(new ContactMeFragment(), "Contact Me");
        viewPager.setAdapter(adapter);
    }
    private static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    private void initProgressBar() {
        progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleLarge);
        progressBar.setIndeterminate(true);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setGravity(Gravity.CENTER);
        relativeLayout.addView(progressBar);
        RelativeLayout.LayoutParams params = new
                RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        progressBar.setVisibility(View.INVISIBLE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        this.addContentView(relativeLayout, params);
    }
    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }
    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toast.makeText(MainActivity.this, "Something went wrong...", Toast.LENGTH_LONG).show();
    }

    @Override
    public void getBackgroundPOJO(BackgroundPOJO backgroundPOJO) {
       txtNickName.setText(backgroundPOJO.getNickName());
       txtDesc.setText(backgroundPOJO.getQuotation());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }


}
