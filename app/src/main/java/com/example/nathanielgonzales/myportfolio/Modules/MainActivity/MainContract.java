package com.example.nathanielgonzales.myportfolio.Modules.MainActivity;

import com.example.nathanielgonzales.myportfolio.Model.BackgroundPOJO;
import com.example.nathanielgonzales.myportfolio.Modules.Background.BackgroundContract;

import java.util.ArrayList;

public interface MainContract {
    interface presenter{
        void onDestroy();
        void getBackgroundDataFromAPI();
    }
    interface MainView {
        void showProgress();
        void hideProgress();
        void onResponseFailure(Throwable throwable);
        void getBackgroundPOJO(BackgroundPOJO backgroundPOJO);
    }
    interface getMainInteractor {
        interface OnFinishedListener {
            void onFinished(BackgroundPOJO backgroundPOJO);
            void onFailure(Throwable t);
        }

        void getBackgroundPOJO(MainContract.getMainInteractor.OnFinishedListener onFinishListener);

        void getBackgroundData(MainContract.getMainInteractor.OnFinishedListener onFinishedListener);
    }

}
