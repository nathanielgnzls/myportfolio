package com.example.nathanielgonzales.myportfolio.Modules.MainActivity;

import com.example.nathanielgonzales.myportfolio.Model.BackgroundPOJO;
import com.example.nathanielgonzales.myportfolio.Network.RetrofitInstance;
import com.example.nathanielgonzales.myportfolio.NetworkApi.APIClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainInteractor implements MainContract.getMainInteractor {
    @Override
    public void getBackgroundPOJO(final OnFinishedListener onFinishListener) {
        APIClient service = RetrofitInstance.getRetrofitInstance().create(APIClient.class);
        Call<BackgroundPOJO> call = service.BackgroundList();
        call.enqueue(new Callback<BackgroundPOJO>() {
            @Override
            public void onResponse(Call<BackgroundPOJO> call, Response<BackgroundPOJO> response) {
                onFinishListener.onFinished(response.body());

                // bpojo = new BackgroundPOJO(response.body().getEmail(),response.body().getLastname(),response.body().getFirstname(),response.body().getNickName(),response.body().getDescription(),response.body().getAge(),response.body().getContactNumber(),response.body().getPosition());
            }

            @Override
            public void onFailure(Call<BackgroundPOJO> call, Throwable t) {
                onFinishListener.onFailure(t);
            }
        });
    }

    @Override
    public void getBackgroundData(OnFinishedListener onFinishedListener) {

    }
}
