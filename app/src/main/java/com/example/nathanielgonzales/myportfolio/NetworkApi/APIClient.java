package com.example.nathanielgonzales.myportfolio.NetworkApi;

import com.example.nathanielgonzales.myportfolio.Model.BackgroundPOJO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface APIClient {

    @GET("myinfo/P9Da3lcf4nkCrFSbiMi7")
    Call<BackgroundPOJO> BackgroundList();
}
